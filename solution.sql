===================================
	MYSQL - CULMINATING ACTIVITY
===================================

--1. Return the costumerName of the customers who are from the Philippines --

	SELECT customerName FROM customers WHERE country = "Philippines";

--2. Return the contactLastName and contactFirstName of costumers with name "La ROchelle Gifts" --

	SELECT contactLastName, contactFirstName FROM customers WHERE customerName = "La Rochelle Gifts";

--3. Return the product name and MSRP of the product named "The Titanic" --

	SELECT productName, MSRP FROM products WHERE productName = "The Titanic";

--4. Return the first and last name of the employee whose email is "jfirrlli@classicmodelcars.com" --

	SELECT firstName, lastName FROM employees WHERE email = "jfirrelli@classicmodelcars.com";

--5. Return the names of costumers who have no registered state --

	SELECT customerName FROM customers WHERE State IS NULL;

--6. Return the first name, last name, email of the employee whose last name is patterson and firstname is Steve --

	SELECT firstName, lastName, email FROM employees WHERE lastName = "Patterson" AND firstName = "Steve";

--7. Return costumer name, country, and credit limit of costumers whose countries are NOT USA and whose credit limit are greater than 3000 --

	SELECT customerName, country, creditLimit FROM customers WHERE country != "USA" AND creditLimit > 3000;

 --8. Return the costumer numbers of orders whose comments contain the string 'DHL' --

 	SELECT customerNumber FROM orders WHERE comments LIKE "%DHL%";

 --9. Return the product lines whose text description mentions the phrase "state of the art" --

 	SELECT productLine FROM productlines WHERE textDescription LIKE "%state of the art%";

 --10. Return the countries of costumers without duplication --

 	SELECT DISTINCT country FROM customers;

 --11. Return the statuses of orders without duplication -- 

 	SELECT DISTINCT status FROM orders;

 --12. Return the costumer names and countries of customers whose country is USA, France , or Canada --

 	SELECT customerName, country FROM customers WHERE country IN ("USA", "France", "Canada");

 --13. Return the first name, last name, and office's city of employees whose offices are in Tokyo --

 	SELECT employees.firstName, employees.lastName, offices.city FROM offices JOIN employees ON offices.officeCode = employees.officeCode WHERE employees.officeCode = 5;

 --14. Return the customer names of customer who were served by the employee named "Leslie Thompson" --

 	SELECT customerName FROM customers WHERE salesRepEmployeeNumber = 1166;

 --15. Return the product name and costumers name of products ordered by "Baane Mini Imports" --

 	SELECT products.productName, customers.customerName FROM products JOIN orderdetails ON products.productCode = orderdetails.productCode JOIN orders ON orderdetails.productCode = orders.customerNumber JOIN customers ON orders.orderNumber = customers.salesRepEmployeeNumber WHERE customers.customerNumber = 121;

 --16. Return the employees'first names, employees'last names, costumers's names, and offices' countries of transaction whose costumers and offices are in the same country --

 	SELECT employees.firstName, employees.lastName, customers.customerName, offices.country FROM customers JOIN employees ON customers.salesRepEmployeeNumber = employees.employeeNumber JOIN offices ON employees.officeCode = offices.officeCode WHERE customers.country = offices.country;

 --17. Return the product name and quantity in stock of products that belong to the product line " planes" with stock quantities less than 1000 --

 	SELECT productName, quantityInStock FROM products WHERE productLine = "Planes" AND quantityInStock < 1000;

 --18. Return the costumer's name with a phone number containing "+81" --

 	SELECT customerName FROM customers WHERE phone LIKE "+81%";



 /Stretch Goal/
1. -- Return the product name of the orders where customer name is Baane Mini Imports
2. --  Return the last name and first name of employees that reports to Anthony Bow
3. -- Return the product name of the product with the maximum MSRP
4. -- Return the number of products group by productline
5. -- Return the number of producs where the status is cancelled

--1. --

SELECT productName FROM products JOIN customers ON products.productCode = customers.salesRepEmployeeNumber JOIN orders ON customers.customerNumber = orders.customerNumber WHERE customerName = "Baane Mini Imports";

--2. --

SELECT employees.firstName, employees.lastName FROM employees WHERE employees.reportsTo = 1143;

--3. --
SELECT productName, MAX(MSRP) FROM products; 

--4. --



--5. --
